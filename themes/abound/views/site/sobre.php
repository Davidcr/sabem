<h1>Bomberos del estado miranda</h1>

<h2>Misión</h2>
<p> Proteger a los habitantes y propiedades del estado, responder a las necesidades de los ciudadanos mediante un rápido, profesional y humanitario servicio, cumpliendo con el compromiso a través de la prevención, combate y extinción  de incendios, servicios de emergencias médicas Pre-Hospitalarias, rescate, educación a la ciudadanía para la autoprotección, atención de desastres y calamidades públicas, técnicas, sociales, naturales; utilizando suficientemente todos los recursos asignados al comando, para proporcionar el mejor servicio a la comunidad.</p>
<Br>

<h2>Visión</h2>
<p>Disponer de una institución capacitada profesionalmente, con funcionarios que tengan una preparación acorde con las exigencias del mundo moderno, a fin de alcanzar los niveles óptimos de calidad y excelencia, para la tranquilidad y satisfacción de la comunidad.</p>
<Br>

<h2>Lema</h2>
<p>Disciplina - Mística - Abnegación</p>
<Br>

<h2>Historia</h2>
<p>Se puede mencionar al año 1952 como la fecha de aparición de lo que vendría a ser la primera institución bomberil mirandina. Años antes se habían realizado múltiples intentos de consolidación de una estación propia, pero no fue sino hasta el 25 de agosto de 1952 que, gracias a una conversación sostenida entre el presidente de la Federación Técnica de Bomberos y el gobernador del estado, Pedro Russo Ferrer, se concretó la creación del Cuerpo de Bomberos del Distrito Sucre.

 

 

El primer directorio estuvo compuesto por:

Pedro Russo Ferrer. Primer jefe

Mayor Antonio González Vidal. Segundo jefe

Capitán Ernesto G. López. Instructor

Capitán Ralph Bazo Viloria. Consultor ingeniero

Capitán Lubin Chacón Escalante. Consultor ingeniero

Capitán Antonio Bonadies. Consultor médico

Capitán Clemente Brito Fernández. Secretario

Teniente Victoriano Jordán Pestana. Subsecretario

Teniente Amadeo Avellaneda. Vocal

Teniente Luis Aguerrevere. Vocal

Subteniente Santiago Giraldi. Jefe Dotación de aspirantes a bomberos voluntarios

La sede de éste primer contingente se limitaba a una oficina en la Junta Comunal de Chacao, lo que hacía necesario realizar los trabajos de entrenamiento en plena calle, específicamente frente a la Iglesia de Chacao. Mientras que la primera dotación estuvo compuesta por un chasis Dodge, un tanque usado de la compañía Shell y una bomba de tres pulgadas; todos ellos adquiridos gracias a la ayuda de las industrias y comercios de la zona.

Para el 01 de mayo de 1954, el aumento de la población y la cantidad de industrias que se habían establecido dentro del territorio del Distrito Sucre, hicieron imprescindibles la iniciación oficial de los servicios de los bomberos, quedando declarada así la primera Guardia Permanente, en un local facilitado como cuartel por la fábrica de muebles “Fénix”, en Los Cortijos de Lourdes”.

Posteriormente, en 1959, el gobernador de la época, Ildemaro Lovera, decretó la extensión a todo el territorio regional de los servicios de este Cuartel de Bomberos. No obstante, dos años más tarde, y debido a la criticada decisión del gobernador Manuel Montilla de nombrar como Comandante General del Cuerpo a Clemente Brito Fernández, el presidente del Concejo Municipal tomara la decisión de desligar a los bomberos de la Gobernación, fundando así el Cuerpo de Bomberos del Distrito Sucre, mejor conocidos como los Bomberos del Este, institución que en 2009 y por disposición del Gobierno Nacional pasa a depender del Ministerio del poder popular de Interior y Justicia, para posteriormente integrarse al Cuerpo de Bomberos del Distrito Capital.
UNA ESTACIÓN PROPIA PARA LOS TEQUES

A principios de los años 60, la ciudad de Los Teques, se vio azotada por una gran cantidad de incendios forestales y algunos incendios estructurales, entre los que destacó el de los archivos del Concejo Municipal, en donde se produjeron pérdidas incalculables debido a la tardanza que hubo al tener que trasladar las unidades de extinción desde la sede de los bomberos de Sucre.

Este y otros siniestros fueron el detonante que impulsó a un grupo de industriales y miembros del Rotary Club de Los Teques a crear una estación bomberil dentro de la ciudad. Fue 14 de julio de 1965, siendo presidente del Concejo Municipal del Distrito Guaicaipuro el Sr. Pedro de La Cruz, cuando se decreta en Gaceta de número Extraordinario la creación del Cuerpo de Bomberos de Los Teques, teniendo como sede el local identificado con el Nº 3 en la calle Carabobo.

La dotación de esta nueva estación se limitaba a una unidad marca Jeep señalada con el número 003, y con la cual atendían los llamados de los vecinos cuando se presentaba algún incendio en la ciudad.

Significativamente, dos años más tarde, el 24 de julio de 1967, salió la primera promoción de bomberos mirandinos, que llevó el nombre de “José Rafael Tosta Materán”, bajo la Orden General Nº 07. La misma estuvo integrada por los efectivos: Carlos García, Carlos Ramírez, Manuel Patiño, Termo José Peña, Argenis Pérez, Valentín Salas, Freddy González, Samuel Hidalgo, Luis Castro, Jesús Pereira, Armando Bermúdez, Timoteo Carrillo, Félix Rodríguez, Orlando Yánez, Freddy Quevedo Quintero, Miguel Peña, Alberto Torres y Santos Vizcaya.

A fines de ese mismo año, la Electricidad de Caracas C.A., el Hospital Policlínico de la ciudad y el Destacamento de la Guardia Nacional, donaron al Cuerpo de Bomberos de Los Teques, un carro escalera, una ambulancia y una camioneta de estacas respectivamente, que unidos a dos carros cisternas que fueron entregados por el Ejecutivo Regional, constituyeron las primeras máquinas de uso bomberil de la época.

Años más tarde, en marzo de 1971, el ciudadano gobernador del estado Miranda Arnaldo Arocha ordenó la unificación de estos bomberos con los de Sucre, quedando el Cuerpo de Los Teques como una sub-estación de los del Distrito Sucre. Pero, una vez más, esto no fue por mucho tiempo, porque en el año 1975, por ordenanzas gubernamentales, la estación de Los Teques pasó a ser dirigida y representada por los Bomberos de Distrito Federal, quedando destacado el Subteniente Carlos Romero Alfonso como jefe de lo que pasó a ser la estación Caribe 12.
 </p>
<Br>